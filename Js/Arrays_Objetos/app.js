function lista() {
    var lista = ["lunes", "martes", "miércoles"]
    console.log("Posicion 2 : ", lista[1]);
    $("#result").append("Posicion 1 : ", lista[0], "<br>Posicion 2 : ", lista[1], "<br>Posicion 3: ", lista[2], "<br>longitud de la lista: ", lista.length);

}

function objeto() {
    var alumno = {
        nombre: "marta",
        edad: 22,
        notas: [8, 6, 7, 8],
        email: "marta@gmail.com"
    };

    $("#result2").append("Nombre: ", alumno.nombre, "<br>Edad: ", alumno.edad, "<br>Email: ", alumno.email);
}


function objetos() {
    var alumno1 = {
        nombre: "pepe",
        edad: 20,
        notwes: [8, 6, 7, 8]
    }
    var alumno2 = {
        nombre: "manolo",
        edad: 22,
        email: "manolo@bombo.es"
    }
    var alumno3 = {
        nombre: "faol",
        edad: 23
    }

    var clase = [alumno1, alumno2, alumno3];
    console.log(clase);
    console.log(clase[1].email);
    // console.log(clase[0].notas[2]);
    var notaroberto = clase[0].notas[2];
    console.log(notaroberto);
}

function propiedadObjeto() {
    var circulo20 = {
        radio: 20,
        area: function () {
            return this.radio * this.radio * Math.PI;
        }
    }
    var circulo40 = {
        radio: 40,
        area: function () {
            return this.radio * this.radio * Math.PI;
        }
    }
    console.log(circulo20.area());
    console.log(circulo40.area());
}

function contador(v){
    for (var c=1; c<(v+1); c++){
        console.log(c);
        $("#result2").append("<br>",c);
      }
}

function muestraT3(cosa){
    $("#result3").append("<br>", cosa);
    console.log(cosa);
}


function showArray(){
    var dias=["lunes", "martes", "miércoles", "jueves", "viernes", "sabado", "domingo"];
    // for (var i=0; i<dias.length; i++){
    //     $("#result2").append("<br>", dias[i]);
    //   }

    for (i in dias){
        $("#result2").append("<br>", dias[i]);
    }

    dias.forEach(a => $("#result").append("<br>", a))

    dias.forEach(muestraT3);

}

function fechaHoy(){
    var hoy = new Date();
    var anyo = hoy.getFullYear();
    var mes = hoy.getMonth();
    var dia = hoy.getDate();
    var diaS = hoy.getDay();
    var diaN=["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sabado"];
    var mesN =["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    
    // console.log(` hoy es dia ${dia} del ${mes} de ${anyo}`);
    console.log("hoy es " + diaN[hoy.getDay()]+" "+ dia + " de "+mesN[mes]+" de "+anyo);
  } 
  
  function diferenciaFechasDias(){
    var fechaInicio = new Date('2019-04-02').getTime();
    var fechaFin    = new Date('2019-04-18').getTime();
    
    var diff = fechaFin - fechaInicio;

    console.log("han pasado " + diff/(1000*60*60*24) + " dias" );
    
    console.log(diff/(1000*60*60*24) );
  }

  diferenciaFechasDias();

  $("#datepickerInicio").datepicker();
  $("#datepickerFin").datepicker();


  //   fechaHoy();
//  lista();
//  objeto();
//  objetos();
// propiedadObjeto()
// contador(20);
// showArray()
