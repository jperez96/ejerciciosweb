USE videogavideogamesvideogamesmes;

/* SELECT */

SELECT Name, Platform, Year, Global_Sales FROM videogames ORDER BY Global_Sales DESC LIMIT 10;

SELECT * FROM videogames ORDER BY Name DESC LIMIT 10;

SELECT COUNT(DISTINCT Platform) FROM videogames;

/* WHERE */

SELECT COUNT(DISTINCT name) AS juegosDistintosPs FROM videogames WHERE Platform LIKE "ps%";

SELECT COUNT(DISTINCT name) AS sportsHastaEl2000 FROM videogames WHERE Year<=2000 AND Genre = "Sports";

SELECT Name, Year FROM videogames WHERE Genre = "Sports" ORDER BY Global_Sales DESC LIMIT 1;

SELECT Name, Year, Global_Sales FROM videogames ORDER BY Global_Sales DESC LIMIT 10;

SELECT Name, Year, Global_Sales FROM videogames WHERE Global_Sales>25 ORDER BY Year;

/* AGREGACION */

select format(sum(global_sales),2, "es_ES") from videogames where platform like "PS%";

select year, count(*) from videogames group by year;

SELECT SUM(Global_Sales) AS ventasTotalesPSP FROM videogames WHERE Platform = "PSP";

SELECT SUM(NA_Sales + EU_Sales) AS ventas_NA_EU_WII FROM videogames WHERE Platform = "wii";

SELECT SUM(NA_Sales) AS ventas_NA_WII , SUM(EU_Sales) AS ventas_EU_WII FROM videogames WHERE Platform = "wii";

SELECT SUM(NA_Sales) AS PS_NA, SUM(EU_Sales) AS PS_Eu, SUM(JP_Sales) AS PS_Jp FROM videogames WHERE platform LIKE "PS%";

SELECT round(SUM(NA_Sales)/325.7, 2) AS IndiceVentas_NA, round(SUM(EU_Sales)/741.3, 2) AS IndiceVentas_EU, round(SUM(JP_Sales)/126.8, 2) AS IndiceVentas_JP FROM videogames;

SELECT round(SUM(NA_Sales),2) AS sumaNA, round(SUM(EU_Sales),2) AS sumaEU, round(SUM(JP_Sales),2) AS sumaJP FROM videogames WHERE Genre IN("Racing", "Sports", "Simulation");

SELECT Genre, round(SUM(NA_Sales),2) AS sumaNA, round(SUM(EU_Sales),2) AS sumaEU, round(SUM(JP_Sales),2) AS sumaJP FROM videogames WHERE Genre IN("Racing", "Sports", "Simulation")GROUP BY Genre;

SELECT Platform, round(SUM(Global_Sales),2) AS total FROM videogames WHERE Year BETWEEN 2008 and 2010 GROUP BY Platform ORDER BY total DESC;

SELECT Year, round(SUM(Global_Sales),2) AS ventas FROM videogames WHERE Genre="Sports" GROUP BY Year;

SELECT Year, round(SUM(Global_Sales),2) AS ventas FROM videogames WHERE Genre="Strategy" GROUP BY Year;

SELECT Name, Genre FROM videogames GROUP BY NAME ORDER BY SUM(NA_Sales+EU_Sales+JP_Sales) DESC LIMIT 10;

SELECT Year, SUM(Global_Sales) AS suma FROM videogames WHERE Genre="Strategy" GROUP BY Year ORDER BY suma DESC LIMIT 1;

SELECT Name AS Nombre, Genre AS Genero FROM videogames GROUP BY NAME ORDER BY SUM(NA_Sales+EU_Sales+JP_Sales) DESC LIMIT 10;

SELECT Platform AS Plataforma, SUM(NA_Sales + EU_Sales + JP_Sales) AS total FROM videogames WHERE Year BETWEEN 2010 and 2015 GROUP BY Platform ORDER BY total DESC limit 5;

SELECT Publisher, SUM(NA_Sales + EU_Sales + JP_Sales) AS total FROM videogames WHERE Year BETWEEN 2010 and 2015 GROUP BY Publisher ORDER BY total DESC limit 5;

SELECT group_concat(distinct(platform)) , year, SUM(NA_Sales + EU_Sales + JP_Sales) AS total FROM videogames WHERE platform in('ps','ps2','ps3','ps4') GROUP BY Platform, year ORDER BY Platform, year;

SELECT * FROM videogames;













