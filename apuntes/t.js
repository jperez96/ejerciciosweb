<Switch>
                <Route exact path="/" component={Home} />
                <Route path="/cursos" render={()=><Cursos contactos={this.state.contactos} />} />
                <Route path="/alumnos" render={() => <Alumnos guardaContacto={this.guardaContacto} />} />
                <Route path="/nuevoalumno" render={(props) => <NuevoAlumno contactos={this.state.contactos} guardaContacto={this.guardaContacto} {...props} />} />
                <Route path="/nuevocurso" render={(props) => <NuevoCurso contactos={this.state.contactos} eliminaContacto={this.eliminaContacto} {...props} />}  />
                <Route component={P404} />
              </Switch>