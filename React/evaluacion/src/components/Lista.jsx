import React from "react";

export default class Lista extends React.Component {

    render() {

        let palabras = this.props.palabras.map(palabra => {
            return (
                <li  key={Math.random()}>{palabra.nombre}</li>   
            );
        })


        return (
            <>
            <ul>
                {palabras}
            </ul>      
            </>
                    
                
        );
    }


}

