
import React, { Component } from 'react';

import { Button, Form, FormGroup, Input, Row} from 'reactstrap';


class Interaccion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
        };

        this.cambioInput = this.cambioInput.bind(this);
        this.submit = this.submit.bind(this);
    }

    //gestión genérica de cambio en campo input
    cambioInput(event) {
        const v =  event.target.value;
        const n = event.target.name;
        this.setState({
            [n]: v
        });
    }

    //método activado al enviar el form (submit)
    submit(e) {
        if(this.state.nombre!==''){
            this.props.nuevaPalabra({
                nombre: this.state.nombre,
            });
        }
        e.preventDefault();
        
    }

 

    render() {

        return (
            <>
            <Form onSubmit={this.submit}>
                <Row>
                        <FormGroup>
                            <Input type="text" 
                                name="nombre" 
                                id="nombreInput"
                                value={this.state.nombre}
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <Button type="submit" color="primary" size="sm">Guardar</Button>
                </Row>
                
            </Form>
            </>
        );
    }
}






export default Interaccion;
