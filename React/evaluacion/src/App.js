import React from 'react';
import './App.css';
import { Button, Row} from 'reactstrap';

// importamos modelo
import Palabra from './components/Palabra';

// importamos los componentes de la aplicación (vistas)
import Interaccion from './components/Interaccion';
import Lista from './components/Lista';

// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';

// clase App 
export default class App extends React.Component {
  constructor(props) {
    super(props);

    const palabrasInit = [
      new Palabra("uno"),
      new Palabra("dos"),
    ];

    this.state = {
      palabras: palabrasInit
    };

    this.nuevaPalabra = this.nuevaPalabra.bind(this);
    // this.eliminaContacto = this.eliminaContacto.bind(this);
     this.saveData = this.saveData.bind(this);
     this.loadData = this.loadData.bind(this);


  }

  nuevaPalabra(datos) {

    // Creamos contacto y lo añadimos a la lista
    let nueva = new Palabra(datos.nombre);
    //añadimos elemento recien creado
    let nuevaLista = this.state.palabras;
    nuevaLista.push(nueva);
    // finalmente actualizamos state
    this.setState({ palabras: nuevaLista });

  }

  saveData() {
    var jsonData = JSON.stringify(this.state);
    localStorage.setItem("datagenda", jsonData);
  }

  //carga de datos
  loadData() {
    var text = localStorage.getItem("datagenda");
    if (text) {
      var obj = JSON.parse(text);
      this.setState(obj);
    }
  }


  render() {

    if (this.state.palabras.length === 0) {
      return <h1>Cargando datos...</h1>;
    }


    return (
      <div className="contenido">
        <Interaccion nuevaPalabra={this.nuevaPalabra} />
        <Lista palabras={this.state.palabras} />
        <br/>
        <Row>
          <Button onClick={this.loadData}>Cargar datos</Button>
          <Button onClick={this.saveData}>Guardar datos</Button>
        </Row>
        <br/>
      </div>
    );

  }

}