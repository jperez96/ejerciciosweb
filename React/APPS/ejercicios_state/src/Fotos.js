import React from 'react';
import "./css/Fotos.css";
import jesko from "./img/jesko.jpg";
import pagani from "./img/pagani.jpg";
import rimac from "./img/rimac.jpg";
import amgGtr from "./img/amgGtr.jpg";
let img;

export default class Fotos extends React.Component {
    constructor(props) {
        super(props);

        this.state = { 
            value: "" 
        }
        this.handleChange = this.handleChange.bind(this);
        img = jesko;
    }
    
    handleChange(event) {
        this.setState({ 
            value: event.target.value });
    }


    render() {
        if (this.state.value === "jesko"){
            img = jesko
        } else if(this.state.value === "pagani"){
            img = pagani
        } else if(this.state.value === "rimac"){
            img = rimac
        } else if(this.state.value === "amgGtr"){
            img = amgGtr
        }

        return (
            <div>
                <select className="selec" value={this.state.value} onChange={this.handleChange}>
                    <option value="jesko">Jesko</option>
                    <option value="pagani">Pagani</option>
                    <option value="rimac">Rimac</option>
                    <option value="amgGtr">AMG GTR</option>
                </select>
                <div>
                <img className="imgSelec" src={img} />
                </div>
            </div>
        );
    }
}