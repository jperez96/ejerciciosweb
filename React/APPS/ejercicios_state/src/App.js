import React from "react";
import Thumbs from "./Thumbs";
import Tricolor from "./Tricolor";
import Fotos from "./Fotos.js";
import Combo from "./Combo.js";
import {CIUTATS_CAT_20K} from './datos.js';

export default () => (
  <>
    
    <Thumbs estadoInicial={true}/>
    <Tricolor estadoInicial="tricolor grey"/>
    <Fotos />
    <Combo campoPrincipal="comarca" campoSecundario="municipi" datos={CIUTATS_CAT_20K} />

    
  </>
);
