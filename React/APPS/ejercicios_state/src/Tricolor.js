import React from 'react';
import './css/Tricolor.css';


export default class Tricolor extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {
            on : this.props.estadoInicial
        }
        this.pulsar = this.pulsar.bind(this);
    }
    
    pulsar(){
        if (this.state.on === "tricolor grey"){
            this.setState({
                on: "tricolor red"
            })
        }else if (this.state.on === "tricolor red"){
            this.setState({
                on: "tricolor green"
            })
        } else if (this.state.on === "tricolor green"){
            this.setState({
                on: "tricolor blue"
            })
        }else if (this.state.on === "tricolor blue"){
            this.setState({
                on: "tricolor grey"
            })
        }
        
    }

    render(){
        return (
            <div onClick={this.pulsar} className={this.state.on}></div>
        );
    }

}