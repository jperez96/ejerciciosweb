import React from 'react';
import "./css/Fotos.css";

export default class Combo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.selecciona=this.selecciona.bind(this);
    }

    selecciona(event) {
        this.setState({
            value: event.target.value
        });
    }



    render() {
        let comarcasRepetidas = this.props.datos.map(el => el.comarca);
        let setDistintas2 = new Set(comarcasRepetidas);
        let inputSuperior = [...setDistintas2].map(item => <option value={item} key={item}>{item}</option>);
        let comarcasSelecionadas = this.props.datos
                     .filter(item => this.state.value === item.comarca )
                     .map(item => <option value={item.municipi} key={item.municipi}>{item.municipi}</option>);

        return (
            <div>
                <select className="selec" value={this.state.value} onChange={this.selecciona}>
                    {inputSuperior}
                </select>
                <select>
                    {comarcasSelecionadas}
                </select>
            </div>
        );
    }
}