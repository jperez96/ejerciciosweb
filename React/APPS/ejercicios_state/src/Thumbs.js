import React from 'react';
import "./css/Thumbs.css";


export default class Thumbs extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {
            on : this.props.estadoInicial
        }
        this.pulsar = this.pulsar.bind(this);
    }
    
    pulsar(){
        this.setState({
            on: !this.state.on
        })
    }

    render(){
        let claseIcono = (this.state.on) ? "fas fa-thumbs-up" : "fas fa-thumbs-down" ;
        return (
            <div onClick={this.pulsar} className="thumbs">
                <i className={claseIcono} />
            </div>
        );
    }

}