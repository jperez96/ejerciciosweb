import React, { Component } from 'react';
import './App.css';
// import Capital from './componentes/Capital';
// import FotoBola from './componentes/FotoBola';
// import Gato from './componentes/Gato';
//import Contador from'./componentes/Contador';
import Calculadora from './componentes/Calculadora';
class App extends Component {
  render() {
    return (
      // <div>
      <React.Fragment>
        {/* <Capital nom="barcelona" />,
          <FotoBola src="http://lorempixel.com/" talla="150" />
        <Gato ancho="200" alto="200" nombre="Garfield" /> */}
        {/* <Contador valorInicial="5"/> */}
        <Calculadora />
      </React.Fragment>
      // </div>
    );
  }
}

export default App;
