import React, { Component } from 'react';
import './css/Gato.css'

function Gato(props) {
    let urlGato = "https://placekitten.com/" + props.ancho + "/" + props.alto;
     return <div className="divGato"  style={{width: props.ancho}}>
    <img src={urlGato} alt="Gato"/>
    <p className="textGato">{props.nombre}</p>
    </div>
}

    export default Gato;