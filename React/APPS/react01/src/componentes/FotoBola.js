import React, { Component } from 'react';

function FotoBola (props){
        let urlBola = props.src + props.talla + "/" + props.talla;
        return <div>
        <img src={urlBola} alt="bola" style={{borderRadius: props.talla/2}}/>
        </div>
    }

    export default FotoBola;