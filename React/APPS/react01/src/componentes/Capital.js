import React, { Component } from 'react';
import './css/Capital.css'

// export default function Capital(props) {
function Capital(props) {
    let inicialCapital = props.nom.substr(0,1).toUpperCase();
    let ciudad = inicialCapital + props.nom.substr(1).toLowerCase();
    return <div className="capital">
    <p>{inicialCapital}</p>
    <p>{ciudad}</p>
    </div>
}

    export default Capital;