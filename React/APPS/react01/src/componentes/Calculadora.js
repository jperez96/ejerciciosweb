// import React from 'react';
import React, { Component } from 'react';
import Boton from './Boton';
import './css/Calculadora.css';

// class Contador extends React.Component {
class Calculadora extends Component {



    constructor(props) {
        super(props);

        this.state = {
            display: "0",
            valorAnterior: 0,
            operacionAnterior: "",
            vaciarDisplay: true
        }

        this.pulsar = this.pulsar.bind(this);
        

    }
    
    pulsar(caracter) {
      if (caracter==="+"){
        this.setState({
            valorAnterior: this.state.display,
            operacionAnterior: "+",
            display: ""
        })
    } else if (caracter==="-"){
        this.setState({
            valorAnterior: this.state.display,
            operacionAnterior: "-",
            display: ""
        })
    }
    else if (caracter==="*"){
        this.setState({
            valorAnterior: this.state.display,
            operacionAnterior: "*",
            display: ""
        })
    }else if (caracter==="/"){
        this.setState({
            valorAnterior: this.state.display,
            operacionAnterior: "/",
            display: ""
        })
    }else if (caracter==="C"){
        this.setState({
            valorAnterior: this.state.display,
            operacionAnterior: "",
            display: "0"
        })
    } else if (caracter==="="){
        let resultado=0;
        if (this.state.operacionAnterior==="+"){
            resultado = this.state.display*1 + this.state.valorAnterior*1;
        }else if (this.state.operacionAnterior==="-"){
            resultado = this.state.valorAnterior*1 - this.state.display*1;
        }else if (this.state.operacionAnterior==="*"){
            resultado = this.state.valorAnterior*1 * this.state.display*1;
        }else if (this.state.operacionAnterior==="/"){
            resultado = this.state.valorAnterior*1 / this.state.display*1;
        }
        this.setState({
            display: resultado,
            vaciarDisplay: true
        })
    } else {
        if (this.state.vaciarDisplay===true){
            this.setState({
                display: caracter,
                vaciarDisplay: false
            });
        }else{
            this.setState({
                display: ""+this.state.display+caracter
            });
        }
        
    }
    }

    createButtons = () => {
        let indiceBotones = ["7", "8", "9", "+", "4", "5", "6", "-", "3", "2", "1", "*", "0", "C", "=", "/"];
        let BotonesGenerados=[];
        // Outer loop to create parent
        for (let i = 0; i < indiceBotones.length; i++) {
            let actual = indiceBotones[i];
            let classBoton = "botonCalc "+"btn"+actual;
            BotonesGenerados.push(<Boton clase ={classBoton} classBoton clicado={() => this.pulsar(actual)} texto={actual}/>);
          }
        return BotonesGenerados
      }



    render() {
        return (
            <div className="calcBody">
                <h3 className="display">{this.state.display}</h3>
                {this.createButtons()}
            </div>
        );
    }

}

export default Calculadora;