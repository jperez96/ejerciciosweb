import React, { Component } from 'react';
import './css/Boton.css';

export default class Boton extends Component {

    constructor(props) {
        super(props);
    }
    
    
    render(){

        return(
            
            <button className={this.props.clase} onClick={this.props.clicado} >{this.props.texto}</button>
        )            
    };
}

