// import React from 'react';
import React, { Component } from 'react';
import Dato from'./Dato';
import Boton from'./Boton';



// class Contador extends React.Component {
class Contador extends Component {

    constructor(props) {
        super(props);

        this.state = {
            display: parseInt(this.props.valorInicial)
            // display: this.props.valorInicial*1
        };

        this.displayMas = this.displayMas.bind(this);
        this.displayMenos = this.displayMenos.bind(this);
    }

    displayMenos() {
        // this.props.valorInicial--; NO HACER ESTO !!!!
        if (this.state.display > 0) {
            this.setState({
                display: this.state.display - 1
            });
        }
    }

    displayMas() {
        // this.props.valorInicial++; NO HACER ESTO !!!!
        if (this.state.display < 8) {
            this.setState({
                display: this.state.display + 1
            });
        }
    }

    render() {
        return (
            <React.Fragment>
                <Boton clicado={this.displayMenos} texto="Menos" />
                {/* <h2>{this.state.display}</h2>
                <h2>{this.state.display+1}</h2>
                <h2>{this.state.display+2}</h2> */}
                <Dato valor={this.state.display} />
                <Dato valor={this.state.display+1} />
                <Dato valor={this.state.display+2} />
                <Boton clicado={this.displayMas} texto="Mas" />
            </React.Fragment>
        );
    }

}

export default Contador;