import React from 'react';
import { BrowserRouter, Switch, Route, NavLink } from "react-router-dom";
import { Navbar, Nav, NavItem, Button } from 'reactstrap';

//importamos CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/app.css';

// importamos los componentes de la aplicación (vistas)
import Home from './components/HomeComponent';
import Cursos from './components/CursosComponent';
import Alumnos from './components/AlumnosComponent';
import NuevoAlumno from './components/NuevoAlumnoComponent';
import NuevoCurso from './components/NuevoCursoComponent';
import P404 from './components/P404';

// importamos Modelos
import Curso from './Models/Curso';
import Alumno from './Models/Alumno';

export default class App extends React.Component {

  constructor(props) {
    super(props);

    const cursosInicio = [
      new Curso(1, "Dev", "informatica"),
      new Curso(2, "Pianista", "Musica"),
      new Curso(3, "Natacion", "Deporte"),
    ]

    const alumnosInicio = [
      new Alumno(1, "Manolo", "manolo@gmail.com", "24", "Mujer"),
      new Alumno(2, "Andres", "andres@gmail.com", "20", "Hombre"),
      new Alumno(3, "Monica", "monica@gmail.com", "22", "Mujer"),
      new Alumno(4, "Ferran", "ferran@gmail.com", "21", "Hombre"),
    ]

    this.state = {
      cursos: cursosInicio,
      ultimoCurso: 3,
      alumnos: alumnosInicio,
      ultimoAlumno: 4
    }

    this.guardaCurso = this.guardaCurso.bind(this);
    this.guardaAlumno = this.guardaAlumno.bind(this);
    this.saveData = this.saveData.bind(this);
    this.loadData = this.loadData.bind(this);
  }

  //extra guardado de datos
  saveData() {
    var jsonData = JSON.stringify(this.state);
    localStorage.setItem("datajsacademy", jsonData);
  }

  //carga de datos
  loadData() {
    var text = localStorage.getItem("datajsacademy");
    if (text) {
      var obj = JSON.parse(text);
      this.setState(obj);
    }
  }


  guardaCurso(datos) {
    //solo si id=0 asignamos nuevo id y actualizamos ultimoCurso
    if (datos.id === 0) {
      datos.id = this.state.ultimoCurso + 1;
      this.setState({ ultimoCurso: datos.id });
    }
    // comprobaciones adicionales... especialidad ya existe? datos llenos?
    // si todo ok creamos curso y lo añadimos a la lista
    let nuevo = new Curso(datos.id, datos.nombre, datos.especialidad);
    // si curso existe lo eliminamos!
    // esto es porque podemos llegar aquí desde nuevo curso o desde modifica curso
    let nuevaLista = this.state.cursos.filter(el => el.id !== nuevo.id);
    //añadimos elemento recien creado
    nuevaLista.push(nuevo);
    // finalmente actualizamos state
    this.setState({ cursos: nuevaLista });

  }

  guardaAlumno(datos) {
    //solo si id=0 asignamos nuevo id y actualizamos ultimoCurso
    if (datos.id === 0) {
      datos.id = this.state.ultimoAlumno + 1;
      this.setState({ ultimoAlumno: datos.id });
    }
    // comprobaciones adicionales... especialidad ya existe? datos llenos?
    // si todo ok creamos curso y lo añadimos a la lista
    let nuevo = new Alumno(datos.id, datos.nombre, datos.email, datos.edad, datos.genero);
    // si curso existe lo eliminamos!
    // esto es porque podemos llegar aquí desde nuevo curso o desde modifica curso
    let nuevaLista = this.state.alumnos.filter(el => el.id !== nuevo.id);
    //añadimos elemento recien creado
    nuevaLista.push(nuevo);
    // finalmente actualizamos state
    this.setState({ alumnos: nuevaLista });

  }

  render() {

    return (
      <BrowserRouter>
        <Navbar color="light" light expand="md">
          <Nav>
            <NavItem>
              <NavLink className="nav-link" to="/">HOME</NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link" to="/cursos">CURSOS</NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link" to="/alumnos">ALUMNOS</NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link" to="/nuevoalumno">NUEVO ALUMNO</NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link" to="/nuevocurso">NUEVO CURSO</NavLink>
            </NavItem>
            <NavItem >
              <Button  className="buttonProgres" onClick={this.loadData}>Cargar Datos</Button>
            </NavItem>
            <NavItem >
              <Button className="buttonProgres" onClick={this.saveData}>Guardar Datos</Button>
            </NavItem>
          </Nav>
        </Navbar>

        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/cursos" render={() => <Cursos cursos={this.state.cursos} />} />
          <Route path="/alumnos" render={() => <Alumnos alumnos={this.state.alumnos} />} />
          <Route path="/nuevoalumno" render={() => <NuevoAlumno guardaAlumno={this.guardaAlumno} />} />
          <Route path="/nuevocurso" render={() => <NuevoCurso guardaCurso={this.guardaCurso} />} />
          <Route component={P404} />
        </Switch>

      </BrowserRouter>


    );
  }
}
