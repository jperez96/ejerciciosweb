import React from "react";

import { Table, Row, Container } from 'reactstrap';

export default class CursosComponent extends React.Component {

   
    render() {


        //para crear las filas hacemos un "map" de los contactos recibidos
        //previamente los ordenamos por id...
        let filas = this.props.cursos.sort((a, b) => a.id - b.id).map(curso => {
            return (
                <tr key={curso.id}>
                    <td>{curso.id}</td>
                    <td>{curso.nombre}</td>
                    <td>{curso.especialidad}</td>
                    {/* <td><Link to={"/modifica/" + curso.id}>Editar</Link></td>
                    <td><Link to={"/elimina/" + curso.id}>Eliminar</Link></td> */}
                </tr>
            );
        })


        return (
            <Container>
                <Row>
                    <Table>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>especialidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filas}
                        </tbody>
                    </Table>
                </Row>
            </Container>
        );
    }


}