import React from "react";

import { Table, Row, Container } from 'reactstrap';

export default class AlumnosComponent extends React.Component {

   
    render() {


        //para crear las filas hacemos un "map" de los contactos recibidos
        //previamente los ordenamos por id...
        let filas = this.props.alumnos.sort((a, b) => a.id - b.id).map(alumno => {
            return (
                <tr key={alumno.id}>
                    <td>{alumno.id}</td>
                    <td>{alumno.nombre}</td>
                    <td>{alumno.email}</td>
                    <td>{alumno.edad}</td>
                    <td>{alumno.genero}</td>
                    {/* <td><Link to={"/modifica/" + alumno.id}>Editar</Link></td>
                    <td><Link to={"/elimina/" + alumno.id}>Eliminar</Link></td> */}
                </tr>
            );
        })


        return (
            <Container>
                <Row>
                    <Table>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>email</th>
                                <th>edad</th>
                                <th>genero</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filas}
                        </tbody>
                    </Table>
                </Row>
            </Container>
        );
    }


}