import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';


class NuevoCursoComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nombre: '',
      especialidad: '',
      volver: false
    };

    this.cambioInput = this.cambioInput.bind(this);
    this.submit = this.submit.bind(this);
  }

  //gestión genérica de cambio en campo input
  cambioInput(event) {
    // const target = event.target;
    // const value = target.type === 'checkbox' ? target.checked : target.value;
    // const name = target.name;
    const value = event.target.value;
    const name = event.target.name;

    this.setState({
      [name]: value
    });
  }

  //método activado al enviar el form (submit)
  submit(e) {
    e.preventDefault();


    this.props.guardaCurso({
      nombre: this.state.nombre,
      especialidad: this.state.especialidad,
      id: 0
    });
    this.setState({ volver: true });
  }

  render() {
    //si se activa volver redirigimos a cursos
    if (this.state.volver === true) {
      return <Redirect to='/cursos' />
    }

    return (

      <Form onSubmit={this.submit}>
        <Row>
          <Col xs="6">
            <FormGroup>
              <Label for="nombreInput">Nombre</Label>
              <Input type="text"
                name="nombre"
                id="nombreInput"
                value={this.state.nombre}
                onChange={this.cambioInput} />
            </FormGroup>
            <FormGroup>
              <Label for="especialidadInput">Especialidad</Label>
              <Input type="text" name="especialidad" id="especialidadInput"
                value={this.state.especialidad}
                onChange={this.cambioInput} />
            </FormGroup>
          </Col>
        </Row>


        <Row>
          <Col>
            <Button type="submit" color="primary">Guardar</Button>
          </Col>
        </Row>
      </Form>

    );
  }
}






export default NuevoCursoComponent;
