import React from "react";

import { Button, Table } from 'reactstrap';
import DataModal from './DataModal';
import { Redirect } from 'react-router-dom';

export default class Lista extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            data: [],
            itemToDelete: 0,
            modalAction: null,
            modalVisible: false,
            modalTitle: '',
            modalBody: '',
            modalBtnOk: '',
            modalBtnNok: '',
            gotoNou: false,
            gotoEdit: 0,
            loadedModel: ''
        }

        this.edit = this.edit.bind(this);
        this.nouRegistre = this.nouRegistre.bind(this);
        this.delete = this.delete.bind(this);
        this.executeDelete = this.executeDelete.bind(this);
        this.getData = this.getData.bind(this);
        this.getData();  //coment
    }


    //nou registre
    nouRegistre(){
        this.setState({gotoNou: true});
    }


    //obté dades del model rebut via props
    getData() {
        this.props.model.getAll()
            .then(data => this.setState({ data }))
            .then(()=>this.setState({loadedModel: this.props.model.getModelName()}))
            .catch(err => console.log(err));
    }

    //configura i mostra modal de confirmació per editar
    edit(id) {
        this.setState({gotoEdit: id});
    }


    //configura i mostra modal de confirmació per eliminar
    delete(id) {
        this.setState({ 
            modalVisible: true, 
            modalAction: this.executeDelete,
            modalParameter: id,
            modalTitle: 'Confirmació',
            modalBody: 'Eliminar registre?',
            modalBtnOk: 'Eliminar',
            modalBtnNok: 'Cancel.lar' });
    }

    executeDelete(resp, id) {
        if (resp === 'ok') {
            let novaData = this.state.data.filter(el => el.id !== id);
            this.setState({ data: novaData });
            this.props.model.deleteById(id);
        }
        this.setState({ modalVisible: false });
    }


    render() {
        // com que utilitzem un mateix component, cal verificar si dades carregades 
        // son les del model solicitat
        // la primera vegada sempre serà així
        if (this.state.loadedModel!==this.props.model.getModelName()){
            this.getData();
            return <></>;
        }
        
        //anem a nou registre si cal
        if (this.state.gotoNou){
            return <Redirect to={`/${this.props.model.getCollection()}/nou`} />  
        }

        //anem a editar registre si cal
        if (this.state.gotoEdit){
            return <Redirect to={`/${this.props.model.getCollection()}/edita/${this.state.gotoEdit}`} />  
        }

        let fieldNames = this.props.model.getFields();
        // titols de les columnes a partir dels camps del model
        let titolsColumnes = fieldNames.map(el =>  <th key={el.name}>{el.name}</th>);
  
        //creem les files a partir de dades rebudes, ordenades per id
        let filas = this.state.data.sort((a, b) => a.id - b.id).map(item => {
                let cols = fieldNames.map(el => <td key={el.name}>{item[el.name]}</td>)
                return (
                    <tr key={item.id}>
                        <td >{item.id}</td>
                        {cols}
                        <td><i onClick={() => this.edit(item.id)} className="fa fa-edit clicable blue-icon" /></td>
                        <td><i onClick={() => this.delete(item.id)} className="fa fa-trash clicable red-icon" /></td>
                    </tr>
                );
            })
        

        return (
            <>
                <h1 className="titol-vista">{this.props.model.getTitle()}</h1>
               <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            {titolsColumnes}
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>

                <Button onClick={this.nouRegistre} >Nou registre</Button>

                <DataModal visible={this.state.modalVisible}
                    title={this.state.modalTitle}
                    body={this.state.modalBody}
                    okText={this.state.modalBtnOk}
                    nokText={this.state.modalBtnNok}
                    action={this.state.modalAction}
                    parameter={this.state.modalParameter}
                     />

            </>
        );
    }


}

