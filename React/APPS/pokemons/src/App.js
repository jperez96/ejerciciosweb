import React from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from "react-router-dom";

//Models
import Pokemon from './Models/Pokemon';

// Components
import Inicio from './Components/Inicio';
import Lista from './Components/Lista';
import Detalle from './Components/Detalle'

// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';

export default class App extends React.Component {

  render() {
    

    return (
      <BrowserRouter>

              <Switch>
                <Route exact path="/" component={Inicio} />
            
                <Route exact path="/pokemons" render={() => <Lista model={Pokemon} />} />
                <Route exact path="/pokemons/:id" render={(props) => <Detalle model={Pokemon}  {...props} />} />} />
                {/* <Route path="/usuaris/edita/:id" 
                    render={(props) => <Edita {...props} model={Usuari} />} />  */}

                {/* <Route component={P404} /> */}
              </Switch>
      </BrowserRouter>
    );

  }

}
