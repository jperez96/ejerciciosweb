import { SERVER } from '../Config';

const MODEL = 'pokemon';
const COLLECTION = MODEL + 's';
const FIELDS = [
    { name: 'nombre', default: '', type: 'text' },
    { name: 'caracter', default: '', type: 'text' },
];

export default class Pokemon {
    constructor(data = {}) {
        this.id = (data.id !== undefined && data.id !== null) ? data.id : 0;
        FIELDS.forEach(el => {
            this[el.name] = (data[el.name] !== undefined && data[el.name] !== null) ? data[el.name] : el.default
        });
    }

    static getTitle() {
        return MODEL + 's';
    }

    static getFields() {
        return FIELDS;
    }

    static getModelName() {
        return MODEL;
    }

    static getCollection() {
        return COLLECTION;
    }

    //getAll demana tots els registres a la API
    static getAll = () => {
        const fetchURL = `${SERVER}/${COLLECTION}`;
        return new Promise((resolve, reject) => {
            fetch(fetchURL)
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => { reject([{ error: err }]); });
        });
    };


    //getById demana un registre a la API
    static getById = (itemId) => {
        const fetchURL = `${SERVER}/${COLLECTION}/${itemId}`;
        return new Promise((resolve, reject) => {
            fetch(fetchURL)
                .then(results => results.json())
                .then(data => resolve(new Pokemon(data)))
                .catch(err => reject([{ error: err }]));
        });
    };

}