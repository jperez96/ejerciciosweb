import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

import Pokemon from '../Models/Pokemon';

class DetallPokemon extends Component {
    constructor(props) {
        super(props);

        //si no rebem paràmetre considerem item nou
        let id = 0;
        if (this.props.match.params.id) {
        id = this.props.match.params.id * 1;
        } 
        console.log(id);

        this.state = {
            data: [],
            goLista: false,
            loadedModel: '',

        }
    
        this.getData = this.getData.bind(this);
        this.volver = this.volver.bind(this);
        
        if (id>0){
            this.getData(id);
        }
        this.getData();
    }

    volver(){
        this.setState({goLista: true});
    }
    
    getData() {
        this.props.model.getById()
            .then(data => this.setState(data[0]))
            // .then(()=>this.setState({loadedModel: this.props.model.getModelName()}))
            .catch(err => console.log(err));
    }


    render() {


        if (this.state.nombre===undefined) {
            return <p>NO ENCONTRADO</p>;
        }

        if (this.state.goLista === true) {
            return <Redirect to={'/pokemons'} />
        }

       

        return (
            <div className="text-center" >
                <h1 className="titol-vista">{"Pokemon " + this.state.nombre}</h1>
                {/* <FotoPokemon nombre={this.state.nombre} /> */}
                <br />
                <br />
                <Button color="primary" onClick={this.volver}>Volver</Button>
                
            </div>
        );
    }
}






export default DetallPokemon;