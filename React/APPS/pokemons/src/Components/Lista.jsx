import React from "react";

import { Table } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';

const ICO = 'https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/'

export default class Lista extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            data: [],
            loadedModel: '',
            details: false
        }
        this.getData = this.getData.bind(this);
        this.getData();
    }

     //obté dades del model rebut via props
     getData() {
        this.props.model.getAll()
            .then(data => this.setState({ data }))
            .then(()=>this.setState({loadedModel: this.props.model.getModelName()}))
            .catch(err => console.log(err));
    }

    render() {
        // com que utilitzem un mateix component, cal verificar si dades carregades 
        // son les del model solicitat
        // la primera vegada sempre serà així
        if (this.state.loadedModel!==this.props.model.getModelName()){
            this.getData();
            return <></>;
        }
        
        //anem a nou registre si cal
        if (this.state.details){
            return <Redirect to={`/${this.props.model.getCollection()}/nou`} />  
        }

        let fieldNames = this.props.model.getFields();
        // titols de les columnes a partir dels camps del model
        let titolsColumnes = fieldNames.map(el =>  <th key={el.name}>{el.name}</th>);

        //creem les files a partir de dades rebudes, ordenades per id
        let filas = this.state.data.sort((a, b) => a.id - b.id).map(item => {
            let cols = fieldNames.map(el => <td key={el.name}>{item[el.name]}</td>)
            return (
                <tr key={item.id}>
                    <td >{item.id}</td>
                    <td><img src={ICO + (item.nombre).toLowerCase() + ".png"} /></td>
                    {cols}
                    <td><Link to={`/${this.props.model.getTitle()}/${item.id}`} className="btn btn-primary">Detalles</Link></td>
                </tr>
            )
        })

        return (
            <>
                <h1 className="titol-vista">{this.props.model.getTitle()}</h1>
               <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>img</th>
                            {titolsColumnes}
                            <th></th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>


            </>
        )
    }
}