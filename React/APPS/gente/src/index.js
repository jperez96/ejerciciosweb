import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
// https://github.com/reactstrap/reactstrap/issues/1285

ReactDOM.render(<App />, document.getElementById("root"));
