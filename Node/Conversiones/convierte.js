
const calculos = require('./calculos');
//console.log(process.argv);

const valor = process.argv[2];
const desde = process.argv[3];
const hacia = process.argv[5];

if (desde==='km' && hacia==="m"){
    console.log(valor + " km son " + calculos.km_m(valor).toFixed(3) + hacia);
    return;
} else if(desde==='km' && hacia==="mi"){
    console.log(valor + " km son " + calculos.km_mi(valor).toFixed(3) + hacia);
    return;
} else if(desde==='m' && hacia==="km"){
    console.log(valor + " m son " + calculos.m_km(valor).toFixed(3) + hacia);
    return;
} else if(desde==='m' && hacia==="mi"){
    console.log(valor + " m son " + calculos.m_mi(valor).toFixed(3) + hacia);
    return;
} else if(desde==='mi' && hacia==="m"){
    console.log(valor + " mi son " + calculos.mi_m(valor).toFixed(3) + hacia);
    return;
} else if(desde==='mi' && hacia==="km"){
    console.log(valor + " mi son " + calculos.mi_km(valor).toFixed(3) + hacia);
    return;
} else if(desde==='km/h' && hacia==="mi/h"){
    console.log(valor + " mi son " + calculos.kmH_miH(valor).toFixed(3) + hacia);
    return;
} else if(desde==='km/h' && hacia==="m/s"){
    console.log(valor + " m son " + calculos.kmH_mS(valor).toFixed(3) + hacia);
    return;
} else if(desde==='mi/h' && hacia==="km/h"){
    console.log(valor + " mi son " + calculos.miH_kmH(valor).toFixed(3) + hacia);
    return;
} else if(desde==='mi/h' && hacia==="m/s"){
    console.log(valor + " m son " + calculos.miH_mS(valor).toFixed(3) + hacia);
    return;
} else if(desde==='m/s' && hacia==="mi/h"){
    console.log(valor + " mi son " + calculos.mS_miH(valor).toFixed(3) + hacia);
    return;
} else if(desde==='m/s' && hacia==="km/h"){
    console.log(valor + " m son " + calculos.mS_kmH(valor).toFixed(3) + hacia);
    return;
}

console.log("Unidades no encontradas");