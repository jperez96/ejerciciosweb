const express = require('express')
const app = express()
const persona = { nombre: "anna", ciutat: "bcn" };
app.get('/', function (req, res) {
    res.send('Hola mundo!')
})
app.get('/person', function (req, res) {
    res.json(persona)
})
app.get('/hola', function (req, res) {
    res.send(` <form method="get" action="/datos">
    Nombre: 
    <input type = "text" name = "nombre" />
    <button type="submit"/>
    </form > `)
    })
app.post('/hola', function (req, res) {
    var name = req.body.nombre;
    //var email = req.body.email;
    res.send(`<h1>Hola ${name}!</h1>`);
})
app.listen(3000, function () {
    console.log('Escuchando en puerto 3000!')
})