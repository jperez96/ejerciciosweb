var express = require('express')
var router = express.Router()
let bodyParser  = require('body-parser');

router.use(bodyParser.urlencoded({
  extended: true
}));

router.get("/", (request, response) => {
  response.send(`
      <form action="/kmh2ms" method="post">
          <label>Entra km/h</label>
          <input name="kmh">
          <input type="submit" value="convertir">
      </form>
  `);
});

router.post("/", (req, res) => {
  console.log(req.body);
  let kmh = req.body.kmh;
  let ms = kmh/3.6;
  res.send(`
      <h2>${kmh} kilometros/h equivalen a ${ms} m/s</h2>
      <a href="/">volver</a>
  `);
});



module.exports = router