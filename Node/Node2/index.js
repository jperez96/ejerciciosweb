let express = require('express');
let app = express();
let bodyParser  = require('body-parser');
let kmh2ms= require('./kmh2ms');

app.use(bodyParser.urlencoded({
    extended: true
  }));

app.get("/", (request, response)=>{
    response.send(`
       <h2>Conversiones</h2>
       <ul>
        <li>
            <a href="/convertir">Selector</a>
        </li>
        <li>
            <a href="/kmh2ms">Kmh2ms</a>
        </li>
        <li>
            <a href="/km">Km a Millas</a>
        </li>
        <li>
            <a href="/m">Km a Metros</a>
        </li>
       </ul>
        `);
})


app.use('/kmh2ms', kmh2ms);

app.get("/km", (request, response)=>{
    response.send(`
        <form action="/millas" method="post">
            <label>Entra Km</label>
            <input name="km">
            <input type="submit">Enviar</input>
        </form>
    `);
})

app.get("/m", (request, response)=>{
    response.send(`
        <form action="/metros" method="post">
            <label>Entra Km</label>
            <input name="km">
            <input type="submit">Enviar</input>
        </form>
    `);
})

app.get("/convertir", (request, response)=>{
    response.send(`
        <form action="/convertir" method="post">
            <label>Entra Km</label>
            <input name="valor">
            
            <select name="unidades">
                <option value="kmM">Km a M</option>
                <option value="KmMi">Km a Mi</option>
          </select> 
          <input type="submit">Enviar</input>
        </form>
    `);
})
.post("/convertir", (request, response)=>{
    let valor = request.body.valor;
    let unidades = request.body.unidades
    response.send(`
        <h2>${valor} ${unidades}</h2>
        <a href="/">volver</a>
    `);
})


app.post("/millas", (request, response)=>{
    let kms = request.body.km;
    let millas = kms*1.6;
    response.send(`
        <h2>${kms} kilometros equivalen a ${millas} millas</h2>
        <a href="/">volver</a>
    `);
})

app.post("/metros", (request, response)=>{
    let kms = request.body.km;
    let metros = kms*1000;
    response.send(`
        <h2>${kms} kilometros equivalen a ${metros} metros</h2>
        <a href="/">volver</a>
    `);
})

// app.post("/convertido", (request, response)=>{
//     let valor = request.body.valor;
//     let unidades = request.body.unidades
//     response.send(`
//         <h2>${valor} ${unidades}</h2>
//     `);
// })



app.listen(3003, () =>{
    console.log("Web online en 3003");
})