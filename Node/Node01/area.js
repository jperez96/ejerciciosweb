
const geom = require('./geometria');

console.log(process.argv);

const figura = process.argv[2];
const valor = process.argv[3];

if (figura=='cuadrado'){
    console.log(`Area de cuadrado de ${valor} de lado: ${geom.areaCuadrado(valor*1)}`);
} else if (figura=='circulo'){
    console.log(`Area de circulo de ${valor} de radio: ${geom.areaCirculo(valor*1)}`);
} else {
    console.log("Error...")
}