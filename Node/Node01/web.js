const http = require('http');
const hostname = 'localhost';
const port = 3000;

const server = http.createServer(
    (req, res) => {
        console.log(req.headers);

        const url = req.url; // /adios /hola

        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html');

        if (url==='/adios'){
            res.end('<html><body><h1>ADIOS!</h1></body></html>');
        } else if (url==='/hola'){
            res.end('<html><body><h1>Hola!</h1></body></html>');
        } else{
            res.end('<html><body><h1>AQUI ESTAMOS!</h1></body></html>');
        }

        
    })

server.listen(port, hostname, () => {
    console.log('TODO OK!!!!!!!!');
    console.log(`Server running at http://${hostname}:${port}/`);
});