function imprimir(cadena) {
    $("#result").append("<br>" + cadena + "<br>");
}

function mayor(a, b) {
    var mayor;

    if (a > b) {
        mayor = (a + " es mayor que " + b);
    } else if (a < b) {
        mayor = (b + " es mayor que " + a);
    } else {
        mayor = "son iguales";
    }
    return mayor;

}

// imprimir(mayor(10,20));

function divisible(valor, dividor) {
    var a = false;
    if (valor % dividor === 0) {
        a = true;
    }
    return a;
}

function divisibles(num) {
    if (divisible(num, 2) == true) {
        imprimir((num + " es par"));
    } else {
        imprimir((num + " No es par"));
    }
    if (divisible(num, 3) == true) {
        imprimir((num + " Si es divisible por 3"));
    } else {
        imprimir((num + " No es divisible por 3"));
    }
    if (divisible(num, 5) == true) {
        imprimir((num + " Si es divisible por 5"));
    } else {
        imprimir((num + " No es divisible por 3"));
    }
    if (divisible(num, 7) == true) {
        imprimir((num + " Si es divisible por 7"));
    } else {
        imprimir((num + " No es divisible por 7"));
    }
}
//   divisibles(1240);

function sumaValores(arr) {
    var suma = 0;
    for (i in arr) {
        suma = (suma + arr[i]);
    }
    return suma
}

// imprimir(sumaValores([1, 3, 9, 2, 3]));

function factorial(x) {
    var total = 1;
    for (i = 1; i <= x; i++) {
        total = total * i;
    }
    return total;
}

// imprimir(factorial(10));

// Pasamos todo el texto a minusculas, y usamos slice para crear una copia del string a partir de la posicion 1
function capitaliza(x) {
    x = x.toLowerCase()
    return x.charAt(0).toUpperCase() + x.slice(1);
}

// imprimir(capitaliza("barna"));
// imprimir(capitaliza("PARIS"));

function palabra(x) {
    var str = "";
    var numeroVocales = 0;
    var numeroConsonates = 0;
    str = ("la palabra " + x + " tiene " + x.length + " letras");
    if (divisible(x.length, 2) == true) {
        str = str.concat("<br>" + x.length + " Es un numero par");
    } else {
        str = str.concat("<br>" + x.length + " Es un numero impar");
    }
    for (var i = 0; i < x.length; i++) {
        letra = x[i];
        if (letra == "a" || letra == "e" || letra == "i" || letra == "o" || letra == "u") {
            numeroVocales = numeroVocales + 1;
        } else {
            numeroConsonates = numeroConsonates +1;
        }

    }
    if (numeroVocales == 1) {
        str = str.concat("<br> El string tiene una vocal");
    } else if (numeroVocales == 0) {
        str = str.concat("<br> El string no tiene vocales");
    } else{
        str = str.concat("<br> El string tiene " + numeroVocales + " vocales");
    }
    if (numeroConsonates == 1) {
        str = str.concat("<br> El string tiene una consonante");
    } else if (numeroConsonates == 0) {
        str = str.concat("<br> El string no tiene consonantes");
    } else {
        str = str.concat("<br> El string tiene " + numeroConsonates + " consonantes");
    }

    // try {
    //     // utilitzamos match para usar una expresion regular, con "i" ignoramos mayus/min ,
    //     // incluimos g para que devuelva un array. Usamos el .length para contar todas.
    //     var numeroVocales = x.match(/[aeiou]/gi).length;
    // }
    // catch (error) {
    //     console.error("No hay vocales en el String!");
    //     str = str.concat("<br> El numero no tiene vocales");

    // }
    // if (numeroVocales == 1) {
    //     str = str.concat("<br> El numero tiene una vocal");
    // } else if (numeroVocales != undefined) {
    //     str = str.concat("<br> El numero tiene " + numeroVocales + " vocales");
    // }
    // try {
    //     // utilitzamos match para usar una expresion regular, con "i" ignoramos mayus/min ,
    //     // incluimos g para que devuelva un array. Usamos el .length para contar todas.
    //     var numeroConsonates = x.match(/([B-D]|[F-H]|[J-N]|[P-T]|[V-Z])/gi).length;
    // }
    // catch (error) {
    //     console.error("No hay consonantes en el String!");
    //     str = str.concat("<br> El numero no tiene consonantes");

    // }
    // if (numeroConsonates == 1) {
    //     str = str.concat("<br> El numero tiene una consonante");
    // } else if (numeroConsonates != undefined) {
    //     str = str.concat("<br> El numero tiene " + numeroConsonates + " consonantes");
    // }
    return str;
}

// imprimir(palabra("HRD"));
// imprimir(palabra("Barcelona"));

function fechaHoy() {
    var hoy = new Date();
    var diaN = ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sabado"];
    imprimir("Hoy es " + diaN[hoy.getDay()]);
    console.log("Hoy es " + diaN[hoy.getDay()]);
}

//   fechaHoy();


function hoyFecha() {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1;
    var yyyy = hoy.getFullYear();

    return yyyy + '-' + mm + '-' + dd;
}

function NavidadFecha() {
    var hoy = new Date();
    var dd = 25;
    var mm = 12;
    var yyyy = hoy.getFullYear();

    return yyyy + '-' + mm + '-' + dd;
}

function navidad() {


    var fechaInicio = new Date(hoyFecha()).getTime();
    var fechaFin = new Date(NavidadFecha()).getTime();

    var diff = fechaFin - fechaInicio;
    diff = diff / (1000 * 60 * 60 * 24);
    diff = Math.trunc(diff);

    imprimir("Faltan " + diff + " dias para navidad");
}

//navidad();

function analiza(arr) {
    var nNum = 0;
    var suma = 0;
    var nMax = arr[0];
    var nMin = arr[0];
    var str = "";
    for (i in arr) {
        suma = (suma + arr[i]);
        nNum++;
        if (nMax < arr[i]) {
            nMax = arr[i]
        }
        if (nMin > arr[i]) {
            nMin = arr[i]
        }
    }
    str = str.concat("La suma de los " + nNum + " números es: " + suma + "<br> El numero mayor es: " + nMax +
        "<br>el numero menor es: " + nMin);

    imprimir(str);
}

// analiza([2,4,8,-2]);

function fibonacci(x) {
    var a = 0;
    var b = 1;
    var str = "";
    console.log(a + "," + b + ",");
    console.log(b);
    str = str.concat(a + "," + b + ",");
    for (i = 2; i < x; i++) {
        c = a + b;
        a = b;
        b = c;
        str = str.concat(c + ",");
        console.log(c + ",");
    }
    imprimir(str);
}

//fibonacci(10)

function primo(x) {

    var NumeroPrimo = true;
    var res;

    for (i = 1; i < x; i++) {
        if (x / i == Math.round(x / i) && i != 1 && i != x) {
            NumeroPrimo = false;
            break;
        };
    };

    if (NumeroPrimo) {
        res = ("El numero " + x + " es Primo");

    } else {
        res = ("El numero " + x + " NO es Primo");
    }
    return res;
}

// imprimir(primo(19));

function primoCifras(x) {
    x = x - 1;
    var j = 1;
    for (i = 1; i <= x; i++) {
        j = j * 10;
    }

    var seguir = "true";
    var numerosPrimos = [];

    for (j; seguir == "true"; j++) {

        if (primoCheck(j)) {
            numerosPrimos.push(j);
            seguir = "false";
            console.log(j);
        }

    }
    imprimir(j - 1);
    console.log("END");

}

function primoCheck(numero) {
    var res = true;
    for (var i = 2; i < numero; i++) {

        if (numero % i === 0) {
            res =  false;
        }

    }

    return res;
}

//  primoCifras(5);